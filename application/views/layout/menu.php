<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavigation"><i class="fa fa-bars"></i></button>
            <img src="<?php echo base_url("public/image/jonathan.png");?>" class="navbar-logo pull-left" alt="" />
            <a href="<?php echo base_url();?>" class="navbar-brand animated flipInX">Matheus Hack</a>
        </div>
        
        <div class="collapse navbar-collapse" id="myNavigation">
            <ul class="nav navbar-nav navbar-right animated flipInX">
                <li><a href="javascript:void(0);" class="scroll btn-sobre-mim">Sobre mim</a></li>
                <li><a href="javascript:void(0);" class="scroll btn-portfolio">Portfólio</a></li>
                <li><a href="javascript:void(0);" class="scroll btn-contato">Contato</a></li>
            </ul>
        </div>
    </div>
</nav>