    <section id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 scrollpoint sp-effect3">
                
               			<ul class="brands brands-inline">
                            <li><a href="#"><i class="fa fa-facebook  hi-icon-effect-8"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>  
                    
                    <p>© Matheus Hack 2014 Todos direitos reservados.</p>
                </div>   	
            </div>
        </div>
    </section>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo base_url("public/js/jquery-latest.min.js");?>"></script>
	<script src="<?php echo base_url("public/js/bootstrap.min.js");?>"></script>
    <script src="<?php echo base_url("public/plugins/creative-brands/creative-brands.js");?>"></script>
    <script src="<?php echo base_url("public/plugins/easy-pie-chart/jquery.easy-pie-chart.js");?>"></script>
    <script src="<?php echo base_url("public/plugins/modernizr/modernizr.custom.js");?>"></script>
    <script src="<?php echo base_url("public/plugins/waypoints/waypoints.min.js");?>"></script>
    <script src="<?php echo base_url("public/js/custom.js");?>"></script>
    <script src="<?php echo base_url("public/js/geral.js");?>"></script>
</body>
</html>