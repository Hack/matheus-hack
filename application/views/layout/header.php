<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Matheus Hack</title>
    <link rel="shortcut icon" href="icons/favicon.ico">
	<link rel="apple-touch-icon" href="icons/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="icons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="icons/apple-touch-icon-114x114.png">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lilita+One' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Alegreya+Sans+SC' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url("public/css/bootstrap.min.css");?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("public/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("public/plugins/animate/animate.css");?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("public/plugins/creative-brands/creative-brands.css");?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("public/plugins/vertical-carousel/vertical-carousel.css");?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url("public/css/custom.css");?>" rel="stylesheet" type="text/css">
    
</head>
<body onload="Scroll();">
	<a href="javascript:void(0);" class="scroll-up scroll btn-topo-site"><i class="fa fa-chevron-up"></i></a>
    <h1>Matheus Hack</h1>
   
    <header>
    	<?php echo $this->load->view("layout/menu");?>
        
        <div class="jumbotron">
            <img src="<?php echo base_url("public/image/jonathan.png");?>" class="img-responsive scrollpoint sp-effect3" alt="" />	
            <div class="social-wrapper">
                <ul class="brands brands-inline hidden-xs scrollpoint sp-effect1">
                    <li><a href="#"><i class="fa fa-facebook  hi-icon-effect-8"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                </ul>   
                <h2 class="scrollpoint sp-effect3">Matheus Hack</h2>
                <ul class="brands brands-inline hidden-xs scrollpoint sp-effect2">
                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                </ul>       
            </div>

            <div id="slideshow" class="carousel slide vertical scrollpoint sp-effect3" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="item active"><h3>Eu <i class="fa fa-heart icon-first"></i> programar</h3></div>
                    <div class="item"><h3>Programador criativo <i class="fa fa-code icon"></i></h3></div>
                </div>
            </div>

        <a href="javascript:void(0);" class="btn btn-reference btn-lg scroll scrollpoint sp-effect1 btn-sobre-mim" role="button"><i class="fa fa-smile-o"></i> Sobre mim</a>
        <a href="javascript:void(0);" class="btn btn-reference btn-lg btn-active scroll scrollpoint sp-effect2 btn-contato" role="button"><i class="fa fa-bolt"></i> Contato</a> 
        </div>
    </header>