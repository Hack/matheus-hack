<section id="testimonials" class="content-first">
    <div class="container">
    	<h2 class="scrollpoint sp-effect3 text-center">Testemunhas</h2>
        <div class="row">
        	
            <div class="col-sm-6 scrollpoint sp-effect1">
                <div class="media">
                    <a class="pull-left" href="#"><img class="media-object img-circle" src="<?php echo base_url("public/image/tom.png");?>" alt=""></a>
                    <div class="media-body">
                        <h4 class="media-heading">John Doe</h4>
                        <span>Webdesigner</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed volutpat est. Donec lobortis interdum volutpat. Maecenas bibendum dui quis pharetra tincidunt. Praesent posuere eu velit at scelerisque.</p>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 scrollpoint sp-effect2">
                <div class="media">
                    <a class="pull-left" href="#"><img class="media-object img-circle" src="<?php echo base_url("public/image/suzanne.png");?>" alt=""></a>
                    <div class="media-body">
                        <h4 class="media-heading">John Doe</h4>
                        <span>Webdesigner</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed volutpat est. Donec lobortis interdum volutpat. Maecenas bibendum dui quis pharetra tincidunt. Praesent posuere eu velit at scelerisque.</p>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</section>