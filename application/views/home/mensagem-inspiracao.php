<section id="quotes" class="content-first">
    <div class="container">
        <div class="row scrollpoint sp-effect3">
            <div class="col-sm-8 col-sm-offset-2">
                <blockquote class="blockquote-reverse">
                    <p><i class="fa fa-quote-left"></i> Qualquer um pode escrever um código que o computador entenda. Bons programadores escrevem códigos que os humanos entendam.</p>
                    <footer> Martin Fowler</footer>
                </blockquote>
            </div>   	
        </div>
    </div>
</section>