<div id="skills">
	<div class="container">   
        <div class="row habilidades-linha">
            <div class="col-md-3">
                <div class="chart" data-percent="75">75%</div>
                <div class="habilidades-descricao"><span>CSS</span></div>
            </div>
            <div class="col-md-3">
                <div class="chart" data-percent="75">75%</div>
                <div class="habilidades-descricao"><span>Html</span></div>
            </div>
            <div class="col-md-3">
                <div class="chart" data-percent="80">80%</div>
                <div class="habilidades-descricao"><span>Jquery</span></div>
            </div>
            <div class="col-md-3">
                <div class="chart" data-percent="60">60%</div>
                <div class="habilidades-descricao"><span>Bootstrap</span></div>
            </div>
        </div>
        <div class="row habilidades-linha">
            <div class="col-md-3">
                <div class="chart" data-percent="80">80%</div>
                <div class="habilidades-descricao"><span>PHP</span></div>
            </div>
            <div class="col-md-3">
                <div class="chart" data-percent="55">55%</div>
                <div class="habilidades-descricao"><span>CakePHP</span></div>
            </div>
            <div class="col-md-3">
                <div class="chart" data-percent="55">55%</div>
                <div class="habilidades-descricao"><span>Codeigniter</span></div>
            </div>
            <div class="col-md-3">
                <div class="chart" data-percent="70">70%</div>
                <div class="habilidades-descricao"><span>MySQL</span></div>
            </div>
        </div> 
        
    </div>
</div>