<section id="showcase" class="content-second">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2 class="scrollpoint sp-effect3">Meu ultimo trabalho</h2>
            </div>
            
            <div class="col-lg-6 showcase-text scrollpoint sp-effect1">
                <div class="row">
                	<div class="col-sm-6 col-md-6 col-lg-12">
                        <div class="media">
                            <div class="pull-left showcase-icon"><i class="fa fa-puzzle-piece"></i></div>
                            <div class="media-body">
                                <h3 class="media-heading margin-ultimo-trabalho">Navegador Amigável</h3>
                                <p>Funciona em todos os tipos de navegadores.</p>
                            </div>
                        </div>
                    </div>
                	<div class="col-sm-6 col-md-6 col-lg-12">
                        <div class="media">
                            <div class="pull-left showcase-icon"><i class="fa fa-trophy"></i></div>
                            <div class="media-body">
                               <h3 class="media-heading margin-ultimo-trabalho">Design de primeira classe</h3>
                                <p>Design moderno e inovador.</p>
                            </div>
                        </div>
                    </div>
                	<div class="col-sm-6 col-md-6 col-lg-12">
                        <div class="media">
                            <div class="pull-left showcase-icon"><i class="fa fa-thumbs-up"></i></div>
                            <div class="media-body">
                                <h3 class="media-heading margin-ultimo-trabalho">Cliente satisfeito</h3>
                                <p>Satisfação do cliente é uma das principais prioridades.</p>
                            </div>
                        </div>
                    </div>
                	<div class="col-sm-6 col-md-6 col-lg-12">
                        <div class="media">
                            <div class="pull-left showcase-icon"><i class="fa fa-lock"></i></div>
                            <div class="media-body" style="padding-left: 38px;">
                                <h3 class="media-heading margin-ultimo-trabalho">Painel Administrativo</h3>
                                <p>Com painel administrativo para controle total do site.</p>
                            </div>
                        </div>
                    </div>
                </div>
                
               <div class="text-center showcase-button">
               		<a class="btn btn-primary btn-lg" role="button" href="http://www.phi.eng.br/" target="_blank"><i class="fa fa-bolt"></i> Veja o site</a>
               </div>
            </div>            
        
            <div class="col-lg-6 col-sm-8 col-md-8 col-md-offset-2 col-sm-offset-2 col-lg-offset-0 showcase-carousel hidden-xs scrollpoint sp-effect2">
                <img src="<?php echo base_url("public/image/ultimo-projeto/imac.png");?>" class="img-responsive" alt="">
                
                <div id="carousel-reference" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active"><img src="<?php echo base_url("public/image/ultimo-projeto/phi-home.png");?>" alt="PHI"></div>
                        <div class="item"><img src="<?php echo base_url("public/image/ultimo-projeto/phi-servicos.png");?>" alt="PHI - Serviços"></div>
                        <div class="item"><img src="<?php echo base_url("public/image/ultimo-projeto/phi-projetos.png");?>" alt="PHI - Projetos"></div>
                        <div class="item"><img src="<?php echo base_url("public/image/ultimo-projeto/phi-contato.png");?>" alt="PHI - Contato"></div>
                    </div>
                    
                    <a class="left carousel-control" href="#carousel-reference" data-slide="prev"><i class="fa fa-arrow-left"></i></a>
                    <a class="right carousel-control" href="#carousel-reference" data-slide="next"><i class="fa fa-arrow-right"></i></a>
                </div>
            </div>  
        </div>
    </div>
</section>