<section id="contact" class="content-first">
    <div class="container">
    	<h2 class="scrollpoint sp-effect3">Contato</h2>
        <div class="row">
            <div class="col-sm-12">
            	<img src="<?php echo base_url("public/image/macbook.png");?>" class="img-responsive hidden-xs" alt="">
                
                <div class="macbook-inner black">
                	<div class="row scrollpoint sp-effect4" id="contato-form">
                    	<div class="col-sm-8">
                        	<h3 class="hidden-xs">Deixe sua mensagem</h3>
                            <form method="POST" onsubmit="return Contato();">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="firstname" class="sr-only">Nome</label>
                                        <input type="text" class="form-control input-contato" placeholder="Nome" name="nome" id="nome">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="lastname" class="sr-only">Sobrenome</label>
                                        <input type="text" class="form-control input-contato" placeholder="Sobrenome" name="sobrenome" id="sobrenome">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="email" class="sr-only">Email</label>
                                        <input type="email" class="form-control input-contato" placeholder="Email" name="email" id="email">
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="message" class="sr-only">Mensagem</label>
                                        <textarea class="form-control input-contato" placeholder="Mensagem" name="mensagem" id="mensagem"></textarea>
                                    </div>
                                    
                                    <div id="btn-salvar">
                                    	<button class="btn btn-primary">Enviar</button>
                                    </div>
                                    
                                </fieldset>
                            </form>
                        </div>
                        <div class="col-sm-4">
                        
                        <img src="<?php echo base_url("public/image/jonathan.png");?>" class="contact-image image-responsive hidden-xs hidden-sm" alt="" />
                        	<div class="contact-info">
                            	<p>Matheus Hack</p>
                                <p>Programador PHP</p>
                                <p>Email: matheus.hack@ig.com.br</p>
                                <p>Celular: (11) 951552816</p>
                            </div>
                        </div>
                	</div>
                </div>
                
            </div>
        </div>
    </div>
</section>