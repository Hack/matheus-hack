<section id="about" class="content-first">
	<div class="container">
		<div class="row">
			<div class="col-sm-2 hidden-xs scrollpoint sp-effect1"> 
				<img src="<?php echo base_url("public/image/jonathan.png");?>" class="image-bordered img-responsive" alt="" />                
			</div>
			
			<div class="col-md-7 col-sm-6 scrollpoint sp-effect1"> 
				<h3>Matheus Hack</h3>
				<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.</p>      
				
				<?php echo $this->load->view("home/twiter");?>
		   	</div> 
			
			<div class="col-md-3 col-sm-4 scrollpoint sp-effect2">
				<?php echo $this->load->view("home/timeline");?>
			</div>
		   
		</div>
		
		
		<div class="row">
			<div class="col-md-12 text-center">
				<a id="skills-toggle" class="btn btn-primary"><i class="fa fa-arrow-circle-o-down"></i> Minhas habilidades</a>
			</div>
		</div>
	</div>
	
	<?php echo $this->load->view("home/habilidades");?>
    
</section>

<?php
	echo $this->load->view("home/ultimo-trabalho");
	echo $this->load->view("home/mensagem-inspiracao");
	echo $this->load->view("home/portfolio");
	echo $this->load->view("home/testemunhas");
	echo $this->load->view("home/contato");
?>