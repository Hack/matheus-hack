<section id="reference" class="content-second">
    <h2 class="scrollpoint sp-effect3">Portfólio</h2>
	<ul class="grid cs-style-4">
            <li>
				<figure class="scrollpoint sp-effect1">
					<div><img src="<?php echo base_url("public/image/portfolio/portal-educacional.png");?>" alt="Portal Educacional Professor Paulo"></div>
					<figcaption>
						<h3>Portal Educacional Professor Paulo</h3>
						<a href="javascript:void(0);" class="em-construcao">Em Construção</a>
					</figcaption>
				</figure>
			</li>

            <li>
				<figure class="scrollpoint sp-effect2">
					<div><img src="<?php echo base_url("public/image/portfolio/fazenda-ouro-branco.png");?>" alt="Fazenda Ouro Branco"></div>
					<figcaption>
						<h3>Fazenda Ouro Branco</h3>
						<a href="javascript:void(0);" class="em-construcao">Em Construção</a>
					</figcaption>
				</figure>
			</li>

            <li>
				<figure class="scrollpoint sp-effect3">
					<div><img src="<?php echo base_url("public/image/portfolio/nathali-ramires.png");?>" alt="Nathali Ramires"></div>
					<figcaption>
						<h3>Nathali Ramires</h3>
						<a href="javascript:void(0);" class="em-construcao">Em Construção</a>
					</figcaption>
				</figure>
			</li>

            <li>
				<figure class="scrollpoint sp-effect1">
					<div><img src="<?php echo base_url("public/image/portfolio/phi-eng.png");?>" alt="PHI Consultoria e Projetos de Impermeabilização"></div>
					<figcaption>
						<h3>PHI Consultoria</h3>
						<a href="http://www.phi.eng.br/" target="_blank">Visualizar</a>
					</figcaption>
				</figure>
			</li>

			<li>
				<figure class="scrollpoint sp-effect2">
					<div><img src="<?php echo base_url("public/image/portfolio/chaos.png");?>" alt="Chaos Online"></div>
					<figcaption>
						<h3>Guia de Hérois Chaos Online</h3>
						<a href="http://chaos.ongame.com.br/guia-de-herois" target="_blank">Visualizar</a>
					</figcaption>
				</figure>
			</li>
			
            <li>
				<figure class="scrollpoint sp-effect3">
					<div><img src="<?php echo base_url("public/image/portfolio/admin-ongame.png");?>" alt="Admin Ongame"></div>
					<figcaption>
						<h3>Admin Ongame</h3>
					</figcaption>
				</figure>
			</li>
            
            <li>
				<figure class="scrollpoint sp-effect1">
					<div><img src="<?php echo base_url("public/image/portfolio/topper-instinct.png");?>" alt="Topper Instinct"></div>
					<figcaption>
						<h3>Topper Instinct</h3>
						<a href="http://www.topper.com.br/futebol-instinct/quiz/">Visualizar</a>
					</figcaption>
				</figure>
			</li>
            
            <li>
				<figure class="scrollpoint sp-effect2">
					<div><img src="<?php echo base_url("public/image/portfolio/bayer-jovens.png");?>" alt="Bayer Jovens"></div>
					<figcaption>
						<h3>Bayer Jovens</h3>
						<a href="http://www.bayerjovens.com.br/pt/home/">Visualizar</a>
					</figcaption>
				</figure>
			</li>
			
            <li>
				<figure class="scrollpoint sp-effect3">
					<div><img src="<?php echo base_url("public/image/portfolio/admin-coca-plas.png");?>" alt="Admin Coca Plas"></div>
					<figcaption>
						<h3>Admin Coca Plas</h3>
					</figcaption>
				</figure>
			</li>
			
		</ul>

</section>