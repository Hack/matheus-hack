<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function _remap($method){
		if(method_exists("Home", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	function __construct()
	{
		parent::__construct();
	}
	
    public function index()
    {
  		$this->load->view('layout/header');
    	$this->load->view('home/index');
		$this->load->view('layout/footer');
    }
	
	public function salvar_contato()
	{
		$envio_email = $this->enviar_email($this->input->post());

		if($envio_email == "email-invalido"){
			echo "<p class='contato-final'>Email inválido!</p>";
		}else if($envio_email == "erro-envio"){
			echo "<p class='contato-final'>Ops, ocorreu algum problema ao enviar seu contato. Por favor tente mais tarde.</p>"; 
		}else{
			echo "<p class='contato-final'>Contato enviado com sucesso. Em breve retornarei seu contato.</p>";
		}
	}
	
	private function enviar_email($dados){
		$this->load->helper('email');

		if(valid_email($dados['email'])){
			 $config = Array(
			  'protocol' => 'smtp',
			  'smtp_host' => 'ssl://smtp.ig.com.br',
			  'smtp_port' => 465,
			  'smtp_user' => 'matheus.hack@ig.com.br',
			  'smtp_pass' => '017803',
			  'mailtype' => 'html',
			  'charset' => 'utf-8',
			  'wordwrap' => FALSE
			);
			
			$data['dados'] = $dados;
			
			$mensagem = $this->load->view("home/email",$data,true);
			$this->load->library('email',$config);
	  		$this->email->from('matheus.hack@ig.com.br' , 'Admin MH');
	  		$this->email->to('matheus.hack@ig.com.br','Matheus Hack'); 
	  		$this->email->subject('[MH] Contato');
	  		$this->email->message($mensagem);
			
	  		if($this->email->send()){
	  			return 'sucesso-envio';
	  		}else{
	  			return 'erro-envio';
	  		}
		}else{
			return 'email-invalido';
	    }

	}

}