function Contato(){
	var status = true;
	$(".input-contato").removeClass("contato-erro");
	
	if($("#nome").val() == ""){
		$("#nome").addClass("contato-erro");
		status = false;
	}
	
	if($("#sobrenome").val() == ""){
		$("#sobrenome").addClass("contato-erro");
		status = false;
	}
	
	if($("#email").val() == ""){
		$("#email").addClass("contato-erro");
		status = false;
	}
	
	if($("#mensagem").val() == ""){
		$("#mensagem").addClass("contato-erro");
		status = false;
	}	
	
	if(status == true){
		$("#btn-salvar").html("<img src='http://localhost/matheushack/public/image/ajax-loader.gif'>");
		$.ajax({
			url: "http://localhost/matheushack/home/salvar_contato",		  
			type: "POST",
			data:{
		  		nome: $("#nome").val(),
		  		sobrenome: $("#sobrenome").val(),
		  		email: $("#email").val(),
		  		mensagem: $("#mensagem").val()
			},
			success:function(result){
				$("#contato-form").html(result);
			}		
		});

		return false;
	}else{
		return false;
	}
	
}

function Scroll()
{
	var pathname = window.location.pathname;
	pathname = pathname.split("/");
	
	if(pathname[2] == "sobre-mim"){
		$("html, body").animate({ scrollTop: 671 }, 10);
	}else if(pathname[2] == "meu-ultimo-trabalho"){
		$("html, body").animate({ scrollTop: 1480 }, 10);
	}else if(pathname[2] == "frase-inspiracao"){
		$("html, body").animate({ scrollTop: 2120 }, 10);
	}else if(pathname[2] == "portfolio"){
		$("html, body").animate({ scrollTop: 2532 }, 10);
	}else if(pathname[2] == "testemunhas"){
		$("html, body").animate({ scrollTop: 3587 }, 10);
	}else if(pathname[2] == "contato"){
		$("html, body").animate({ scrollTop: 4106 }, 10);
	}
}

$(document).ready(function(){
	
	
	$(window).scroll(function() {
		console.log($(this).scrollTop());
		
		var scroll = $(this).scrollTop();
		
		if(scroll > 4040){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/contato/');
		}else if(scroll > 3560){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/testemunhas/');
		}else if(scroll > 2440){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/portfolio/');
		}else if(scroll > 2080){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/frase-inspiracao/');
		}else if(scroll > 1440){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/meu-ultimo-trabalho/');
		}else if(scroll > 640){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/sobre-mim/');
		}else if(scroll < 600){
			window.history.replaceState('Object', 'Matheus Hack', '/matheushack/');
		}
		
	});
	
	
	$(".btn-topo-site").click(function(){
		window.history.replaceState('Object', 'Matheus Hack', '/matheushack/');
		$("html, body").animate({ scrollTop: 0 }, 1000);
    	return false;
	});	
	
	$(".btn-sobre-mim").click(function(){
		window.history.replaceState('Object', 'Matheus Hack', '/matheushack/sobre-mim/');
		$("html, body").animate({ scrollTop: 671 }, 1000);
    	return false;
	});
	
	$(".btn-portfolio").click(function(){
		window.history.replaceState('Object', 'Matheus Hack', '/matheushack/portfolio/');
		$("html, body").animate({ scrollTop: 2532 }, 1000);
    	return false;
	});
	
	$(".btn-contato").click(function(){
		window.history.replaceState('Object', 'Matheus Hack', '/matheushack/contato/');
		$("html, body").animate({ scrollTop: 4106 }, 1000);
    	return false;
	});	

});
